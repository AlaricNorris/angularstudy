import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { DemoNgZorroAntdModule } from './ng-zorro-antd.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DemoNgZorroAntdModule,
    NzButtonModule,
    FormsModule,
  ],
  exports: [
    CommonModule,
    DemoNgZorroAntdModule,
    NzButtonModule,
    FormsModule,
  ]
})
export class ShareModule { }
