import { Component, OnInit } from '@angular/core';
import { User } from '../Models';
import { UserService } from '../user.service';
import { ngxLoadingAnimationTypes } from 'ngx-loading';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public ngxLoadingAnimationTypes = ngxLoadingAnimationTypes;
  users: User[] = [];
  loading = false;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUsers();
  }
  // getUsers(): void {
  //   this.users = this.userService.getUsers();
  // }
  getUsers(): void {

    this.loading = true;
    this.userService.getUsers()
      .subscribe((res) => {

        this.loading = false;
        console.log("all values", JSON.stringify(res));
        this.users = res["data"]["list"];
      },
        (err) => {
          this.loading = false;
          //...
        }
      );
  }

}
