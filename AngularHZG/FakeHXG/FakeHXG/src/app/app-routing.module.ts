import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TodoListComponent } from './todo-list/todo-list.component';
import { UserComponent } from './user/user.component';
import {UserDetailComponent} from './user-detail/user-detail.component';

const routes: Routes = [
  {path: 'user', component: UserComponent},
  {path: 'detail', component: UserDetailComponent},
  {path: 'detail/:id', component: UserDetailComponent},
  {path: 'todo', component: TodoListComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
