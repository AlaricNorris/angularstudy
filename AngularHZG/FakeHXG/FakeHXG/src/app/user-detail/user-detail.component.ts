import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../Models';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  users: User[] = [];
  user: User = {
    id: 0,
    name: '',
    gender: false,
    addr: ''
  };
  constructor(
    private route: ActivatedRoute,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.getHero();
  }
  getHero(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.userService.getUsers()
      .subscribe(res => {
        this.users = res["data"]["list"];
        var asdf = this.users.find(a => a.id + '' === id);
        this.user = JSON.parse(JSON.stringify(asdf));
      });
  }
}
