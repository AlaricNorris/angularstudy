import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

import { USERS } from './mock.data';
import { User } from './Models';
@Injectable({
  providedIn: 'root'
})
export class UserService {


  // userUrl = 'https://www.fastmock.site/mock/37666f9c160ab4b9182191952fa5b988/cs/users';
  userUrl = 'https://www.fastmock.site/mock/b5b91a678744ef07d4cec18837b58ca7/genshin/api/users';
  constructor(private http: HttpClient) { }

  getUsers(): Observable<any> {
    return this.http.get<any>(this.userUrl);
  }
}
