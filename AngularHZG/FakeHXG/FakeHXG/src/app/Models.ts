export interface Todo {
    id: number;
    name: string;
}
export class User {
    id: number = 0;
    name: string = "";
    gender: boolean = true;
    addr: string = "";
}
