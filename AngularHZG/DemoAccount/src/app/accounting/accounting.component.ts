import { Component, OnInit } from '@angular/core';


export interface BillType {
  name: string;
  id: number;
}

@Component({
  selector: 'app-accounting',
  templateUrl: './accounting.component.html',
  styleUrls: ['./accounting.component.css']
})

export class AccountingComponent implements OnInit {
  money = ''; // 金额
  billTypes: BillType[] = []; // 记账类型
  contentStyle = {   // 绑定的样式
    'overflow': 'scroll',
    'height': window.screen.availHeight - 145 + 'px'
  };

  constructor() {
    let n = 0;
    while (n < 20) { // 模拟一些数据
      this.billTypes.push({ name: '食物', id: n });
      n++;
    }
  }
  ngOnInit() {

  }

}