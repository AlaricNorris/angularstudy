import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ChangeDetectionStrategy
} from '@angular/core';
import { TabItem } from '../../domain';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent implements OnInit {
  @Output() tabSelected = new EventEmitter<TabItem>();
  tabItems: TabItem[] = [
    {
      title: '首页',
      icon: '/assets/tabs/home.png',
      link: 'home',
      selectedIcon: '/assets/tabs/home_selected.png'
    },
    {
      title: '全部商品',
      icon: '/assets/tabs/recommend.png',
      link: 'goods',
      selectedIcon: '/assets/tabs/recommend_selected.png'
    },
    {
      title: '汇赚钱',
      icon: '/assets/tabs/category.png',
      link: 'earn',
      selectedIcon: '/assets/tabs/category_selected.png'
    },
    {
      title: '购物车',
      icon: '/assets/tabs/chat.png',
      link: 'shopcart',
      selectedIcon: '/assets/tabs/chat_selected.png'
    },
    {
      title: '我的',
      icon: '/assets/tabs/my.png',
      link: 'my',
      selectedIcon: '/assets/tabs/my_selected.png'
    }
  ];
  @Input() selectedIndex = 0;
  constructor() {}

  ngOnInit() {}

  toggleSelected(idx: number) {
    this.selectedIndex = idx;
    this.tabSelected.emit(this.tabItems[this.selectedIndex]);
  }
}
