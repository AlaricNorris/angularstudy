import { Directive, ElementRef, HostBinding, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
    selector: '[appGridItemTitle]',
})
export class GridItemTitleDirective {
    @HostBinding('style.grid-area') area = 'title';
    @Input() @HostBinding('style.font-size') appGridItemTitle = '1rem';

}