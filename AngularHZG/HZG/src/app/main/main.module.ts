import { NgModule, InjectionToken } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [],
  // 传统写法，如果采用这种写法，就不能在 service 中写 `providedIn`
  providers: [],
  imports: [
    SharedModule,
  ]
})
export class MainModule {


  constructor() { }
}
