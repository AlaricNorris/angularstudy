import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { MatSliderModule } from '@angular/material/slider';
import { MatSnackBar } from '@angular/material/snack-bar';

import { WeUiModule, ButtonConfig } from 'ngx-weui';
import { HomeComponent } from './module/home/home.component';



// export function buttonConfig() {
//   return Object.assign(new ButtonConfig(), { type: 'warn' });
// }

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    HomeComponent
  ],
  imports: [
    SharedModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    // WeUiModule,
  ],
  providers: [
      // // 重置默认按钮样式为：warn
      // { provide: ButtonConfig, useFactory: buttonConfig },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
