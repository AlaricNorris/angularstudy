import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './module/home/home.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'goods', component: HomeComponent},
  {path: 'earn', component: HomeComponent},
  {path: 'shopcart', component: HomeComponent},
  {path: 'my', component: HomeComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
