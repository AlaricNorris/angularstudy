import { Component, ViewChild } from '@angular/core';
import { Channel, ImageSlider, ImageSliderComponent, TopMenu } from './shared/components';

interface Dict {
  [key: string]: string
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FcukPDD';
  scrollTabBkgColor = '#00000000';
  topMenus: TopMenu[] = [
    { title: '垃圾', link: '#' },
    { title: '狗屎', link: '#' },
    { title: '夕夕', link: '#' },
    { title: '狗曰', link: '#' },
    { title: '沙雕', link: '#' },
    { title: '歹歹', link: '#' },
    { title: '憨批', link: '#' },
    { title: '司马', link: '#' },
    { title: '并多', link: '#' },
    { title: '曰狗', link: '#' },
    { title: '鲨毙', link: '#' },
    { title: '夕多', link: '#' },
    { title: '垃圾', link: '#' },
    { title: '狗屎', link: '#' },
    { title: '歹歹', link: '#' },
  ];
  topMenus2: TopMenu[] = [
    { title: '不知好歹', link: '#' },
  ];
  imageSliders: ImageSlider[] = [
    {
      imgUrl: 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.jj20.com%2Fup%2Fallimg%2F1113%2F031H0111515%2F20031G11515-9-1200.jpg&refer=http%3A%2F%2Fimg.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1645173503&t=1b74619cffd52c489f7c5b7725d790b9',
      link: '',
      caption: '垃圾',
    },
    {
      imgUrl: 'https://img2.baidu.com/it/u=144586780,3628411690&fm=253&fmt=auto&app=138&f=JPEG?w=800&h=500',
      link: '',
      caption: '夕夕',
    },
    {
      imgUrl: 'https://img2.baidu.com/it/u=220990409,124547830&fm=253&fmt=auto&app=138&f=JPEG?w=889&h=500',
      link: '',
      caption: '鲨毙',
    },
    {
      imgUrl: 'https://img0.baidu.com/it/u=535862246,1009430706&fm=253&fmt=auto&app=138&f=JPEG?w=889&h=500',
      link: '',
      caption: '并歹',
    },
  ];

  @ViewChild('imageSlider')
  imgSlider!: ImageSliderComponent;



  channels: Channel[] = [
    {
      id: 1,
      title: "热点多多",
      icon: "https://img.pddpic.com/goods/2020-04-29/798a43c5ae721a5d3dbbcbd5f95488db.png?imageView2/2/w/117/q/80/format/webp",
      link: "hot"
    },
    {
      id: 2,
      title: "新品多多",
      icon: "https://commimg.pddpic.com/oms_img_ng/2022-02-13/0ad55eff-1caa-45c5-9c99-6774d8244644.png?imageView2/2/w/117/q/80/format/webp",
      link: "new"
    },
    {
      id: 3,
      title: "秒杀多多",
      icon: "https://img.pddpic.com/goods/2020-04-19/ecdc795c12e256a869a883f247ccdc5d.png?imageView2/2/w/117/q/80/format/webp",
      link: "spike"
    },
    {
      id: 4,
      title: "发现多多",
      icon: "https://img.pddpic.com/goods/2020-01-14/b39df4af9b17ba0d063c04da0aea85aa.png?imageView2/2/w/117/q/80/format/webp",
      link: "reposit"
    },
    {
      id: 5,
      title: "特卖多多",
      icon: "https://img.pddpic.com/goods/2020-01-14/8ed387bd5d07f45ce6fee30a0ab80e80.png?imageView2/2/w/117/q/80/format/webp",
      link: "hot"
    },
    {
      id: 6,
      title: "消灭多多",
      icon: "https://img.pddpic.com/goods/2020-01-14/37fe68d866cb25a887beeb74699196b9.png?imageView2/2/w/117/q/80/format/webp",
      link: "hot"
    },
    {
      id: 7,
      title: "抢杀多多",
      icon: "https://commimg.pddpic.com/oms_img_ng/2022-02-13/b94dc841-5e0f-4fe9-ac82-42a54d982719.gif?imageView2/2/w/117/q/80/format/webp",
      link: "hot"
    },
    {
      id: 8,
      title: "阿斯顿发",
      icon: "https://commimg.pddpic.com/oms_img_ng/2022-02-13/b94dc841-5e0f-4fe9-ac82-42a54d982719.gif?imageView2/2/w/117/q/80/format/webp",
      link: "hot"
    },
    {
      id: 9,
      title: "阿斯顿发",
      icon: "https://commimg.pddpic.com/oms_img_ng/2022-02-13/b94dc841-5e0f-4fe9-ac82-42a54d982719.gif?imageView2/2/w/117/q/80/format/webp",
      link: "hot"
    },
    {
      id: 10,
      title: "阿斯顿发",
      icon: "https://commimg.pddpic.com/oms_img_ng/2022-02-13/b94dc841-5e0f-4fe9-ac82-42a54d982719.gif?imageView2/2/w/117/q/80/format/webp",
      link: "hot"
    },
    {
      id: 10,
      title: "阿斯顿发",
      icon: "https://commimg.pddpic.com/oms_img_ng/2022-02-13/b94dc841-5e0f-4fe9-ac82-42a54d982719.gif?imageView2/2/w/117/q/80/format/webp",
      link: "hot"
    },
    {
      id: 10,
      title: "阿斯顿发",
      icon: "https://commimg.pddpic.com/oms_img_ng/2022-02-13/b94dc841-5e0f-4fe9-ac82-42a54d982719.gif?imageView2/2/w/117/q/80/format/webp",
      link: "hot"
    },
    {
      id: 10,
      title: "阿斯顿发",
      icon: "https://commimg.pddpic.com/oms_img_ng/2022-02-13/b94dc841-5e0f-4fe9-ac82-42a54d982719.gif?imageView2/2/w/117/q/80/format/webp",
      link: "hot"
    },
    {
      id: 10,
      title: "阿斯顿发",
      icon: "https://commimg.pddpic.com/oms_img_ng/2022-02-13/b94dc841-5e0f-4fe9-ac82-42a54d982719.gif?imageView2/2/w/117/q/80/format/webp",
      link: "hot"
    },
  ];


  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    console.log('AppComponent ngAfterViewInit',this.imgSlider);
  }




  handleTabSelecetd(topMenu: { index: String, menu: TopMenu }) {
    // const colors = ['red', 'blue', 'gray'];
    // const index = Math.floor(Math.random() * 3);
    // this.scrollTabBkgColor = colors[index];
    console.log(topMenu);
    console.log(topMenu.menu);
  }

  // constructor() {
  //   console.log(this.dict['a']);
  // }


  // dict: Dict = {
  //   a: "1",
  //   b: "2",
  //   c: "3",
  //   d: "4",
  // }
  // add: AddFunc = (x, y) => x + y;
  // addd: AddFunc = (x, y) => { return x + y };
}

interface AddFunc {
  (x: number, y: number): number
}