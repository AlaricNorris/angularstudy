export function Emoji() {
    return function (target: any, key: string) {


        let val = target[key];

        const getter = () => {
            return val;
        }
        const settter = (value: String) => {
            val = '🤣' + value + '🤣';
        }

        Object.defineProperty(target, key, {
            get: getter,
            set: settter,
            enumerable: true,
            configurable: true,
        });
    };
}


export function Confirmable(message: string) {
    return function (target: any, key: string, descriptor: PropertyDescriptor) {

        const original = descriptor.value;
        descriptor.value = function (...args: any) {
            const allow = window.confirm(message);
            if (allow) {
                const result = original.apply(this, args);
                return result;
            }
            return null;
        };
    };
}