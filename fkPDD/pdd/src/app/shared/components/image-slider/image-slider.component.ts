import { AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, QueryList, Renderer2, ViewChild, ViewChildren } from '@angular/core';

export interface ImageSlider {
  imgUrl: string;
  link: string;
  caption: string;
}

@Component({
  selector: 'app-image-slider',
  templateUrl: './image-slider.component.html',
  styleUrls: ['./image-slider.component.css']
})
export class ImageSliderComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() sliders: ImageSlider[] = [];
  @Input() sliderHeight = '160px';
  @Input() intervalBySeconds = 2;

  selectedIndex = 0;
  intervalId: any;

  @ViewChild('imageSlider', { static: true })
  imageSliderViewChild!: ElementRef;
  @ViewChildren('img')
  imgs!: QueryList<ElementRef>;

  constructor(private rd2: Renderer2) { }

  ngOnInit() {
    console.log('ngOnInit', this.imageSliderViewChild);
    console.log('ngOnInit', this.imgs);
    // this.imageSliderViewChild.nativeElement.innerHTML = '<span>hello</span>';
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    console.log('ngAfterViewInit', this.imgs);
    // this.imgs.forEach(item => item.nativeElement.style.height = '100px');
    this.imgs.forEach(item => {
      this.rd2.setStyle(item.nativeElement, 'height', this.sliderHeight)
    });

    this.setInterval();
  }

  setInterval() {
    this.intervalId = setInterval(() => {
      this.rd2.setProperty(
        this.imageSliderViewChild.nativeElement,
        'scrollLeft',
        (this.getIndex(++this.selectedIndex) % this.sliders.length)
        * this.imageSliderViewChild.nativeElement.scrollWidth
        / this.sliders.length);
    }, this.intervalBySeconds * 1000);
  }

  cancelInterval() {
    clearInterval(this.intervalId);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.cancelInterval();
  }


  getIndex(index: number): number {
    return index >= 0
      ? index % this.sliders.length
      : this.sliders.length - (Math.abs(index) % this.sliders.length);
  }


  handleScroll(event: any) {
    const ratio =
      (event.target.scrollLeft * this.sliders.length) / event.target.scrollWidth;
    this.selectedIndex = Math.round(ratio);
  }
  handleDrag(event: Event) {
    console.log('asdfasdf');
    // const ratio =
    //   (event.target.scrollLeft * this.sliders.length) / event.target.scrollWidth;
    // this.selectedIndex = Math.round(ratio);
  }
  handleDown(event: Event) {
    console.log('down');
    // const ratio =
    //   (event.target.scrollLeft * this.sliders.length) / event.target.scrollWidth;
    // this.selectedIndex = Math.round(ratio);
  }
  handleUp(event: Event) {
    console.log('up');
    // const ratio =
    //   (event.target.scrollLeft * this.sliders.length) / event.target.scrollWidth;
    // this.selectedIndex = Math.round(ratio);
  }
  handleEnter(event: Event) {
    console.log('enter');
    // const ratio =
    //   (event.target.scrollLeft * this.sliders.length) / event.target.scrollWidth;
    // this.selectedIndex = Math.round(ratio);
  }
}
