import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';

export interface TopMenu {
  title: string;
  link: string;
}
@Component({
  selector: 'app-scrollable-tab',
  templateUrl: './scrollable-tab.component.html',
  styleUrls: ['./scrollable-tab.component.css']
})
export class ScrollableTabComponent implements OnInit
// , 
// OnChanges, OnDestroy, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked 
{
  @Input() bkgColor = '#00000000';
  @Input() titleActiveColor = 'blue';
  @Input() titleColor = 'yellow';
  @Input() indicatorColor = 'brown';
  @Input() menus: TopMenu[] = [];
  @Output() tabSelected = new EventEmitter();

  selectedIndex = 0;

  constructor() {
    console.log('ScrollableTabComponent', 'constructor');
  }

  ngOnInit(): void {
    console.log('ScrollableTabComponent', 'ngOnInit');
  }
  // ngOnDestroy(): void {
    

  //   console.log('ngOnDestroy');
  // }

  // /**
  //  * 
  //  * @param changes 
  //  */
  // ngOnChanges(changes: SimpleChanges): void {
  //   //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
  //   //Add '${implements OnChanges}' to the class.

  //   console.log('输入属性改变', changes);
  // }

  // ngAfterContentInit(): void {
  //   //Called after ngOnInit when the component's or directive's content has been initialized.
  //   //Add 'implements AfterContentInit' to the class.

  //   console.log('组件内容初始化');
  // }

  // ngAfterContentChecked(): void {
  //   //Called after every check of the component's or directive's content.
  //   //Add 'implements AfterContentChecked' to the class.
  //   console.log('组件内容脏值检测');
  // }
  // ngAfterViewInit(): void {
  //   //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
  //   //Add 'implements AfterViewInit' to the class.
  //   console.log('组件视图初始化');
  // }

  // ngAfterViewChecked(): void {
  //   //Called after every check of the component's view. Applies to components only.
  //   //Add 'implements AfterViewChecked' to the class.
  //   console.log('组件视图脏值检测');
  // }

  // ngDoCheck(): void {
  //   //Called every time that the input properties of a component or a directive are checked. Use it to extend change detection by performing a custom check.
  //   //Add 'implements DoCheck' to the class.

  //   console.log('ngDoCheck');
  // }


  handleSelection(index: number) {
    this.selectedIndex = index;
    this.tabSelected.emit({
      index: this.selectedIndex,
      menu: this.menus[this.selectedIndex],
    });
  }
}
